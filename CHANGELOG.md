# Change Log
Todos los cambios del proyecto se documentarán en este fichero.

[Información adicional](https://keepachangelog.com/es-ES/1.0.0/)

## [0.0.1-SNAPSHOT] - 20/10/2019
-----------------
### Added

* Inclusion de suma de dos enteros a partir de una llamada rest.
* Inclusión de resta de dos enteros a partir de una llamada rest.