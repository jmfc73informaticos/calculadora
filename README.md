# Informacion de la calculadora

### Entorno de trabajo

* Eclipse IDE for Enterprise Java Developers. Version: 2019-06 (4.12.0)
* Java jdk1.8.0_221
* Maven 3.6.2

### Librerias incluidas en el proyecto
 
 * Aparte del tracer aportado y las propias que incorporamos con spring-boot se usa lombok, versión 1.18.10, por lo que facilita el trabajo a la hora de generar getter, setter, constructores, tareas de logueo, etc 

### Información adicional
 
* Aunque se pide que los operandos se pasen por parametro he optado por pasarlos en el body porque me parece más potente, ya que la calculadora está en su primera versión.
* Se ha creado una carpeta documentación en la que se incluye un json con las invocaciones a los endpoints, tanto de la suma, como de la resta. Ha sido exportada desde Postman. 
* Se han incluido trazas de entrada de datos y de salida(pedido) para facilitar la revisión y por no ser datos sensibles o confidenciales. Esto habría que tenerlo en cuenta configurando el log en los diferentes entornos o casí mejor cifrando la información.  
* También se incluye un archivo CHANGELOG con la información de la versión 
* Se puede ver el contrato del microservicio y probarlo en [swagger](http://localhost:8080/swagger-ui.html) al arrancar el proyecto

### Generación del jar.

* Inclusión de los tracer proporcionados en el repositorio de maven.
* Creamos la ruta(\io\corp\calculator\tracer\1.0.0)
* Incluimos en la carpeta 1.0.0 los jars proporcionados
* Inclusión en el pom de la dependencia que se muestra más abajo.
			
```
		<dependency>
			<groupId>io.corp.calculator</groupId>
			<artifactId>tracer</artifactId>
			<version>1.0.0</version>
		</dependency>
```
* Inclusion en el pom de un profile para indicar el empaquetado:

```
	<profiles>
	   <profile>
	       <id>jar</id>
	       <properties>
	           <packaging.type>jar</packaging.type>
	       </properties>
	   </profile>
   </profiles>
```

* Ahora compilamos en proyecto con "mvn clean package -Pjar" situandonos en la ruta del proyecto

```
..\calculadora>mvn clean package -Pjar
```
	
	
### Arrancar el proyecto

>Es suficiente con situarnos en la carpeta target y ejecutar "java -jar calculadora-0.0.1-SNAPSHOT.jar"

```
..\calculadora\target>java -jar calculadora-0.0.1-SNAPSHOT.jar
```
