package com.prueba.calculadora.api;

import com.prueba.calculadora.dto.entrada.RestaDto;
import com.prueba.calculadora.dto.entrada.SumaDto;

/**
 * Api con las operaciones para una calculadora
 * 
 */
public interface CalculadoraApi {

	/**
	 * Metodo que suma dos numeros enteros
	 * 
	 * @param sumaDto
	 *            contiene el sumando1 y el sumando2
	 * @return suma de los sumandos
	 */
	String suma(SumaDto sumaDto);

	/**
	 * Metodo que resta dos numeros enteros
	 * 
	 * @param restaDto
	 *            incluye el minuendo y el sustraendo de una resta
	 * @return resta de minuendo y sustraendo
	 */
	String resta(RestaDto restaDto);

}