package com.prueba.calculadora.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.calculadora.api.CalculadoraApi;
import com.prueba.calculadora.dto.entrada.RestaDto;
import com.prueba.calculadora.dto.entrada.SumaDto;
import com.prueba.calculadora.services.CalculadoraService;
import com.prueba.calculadora.services.utils.UtilsTrace;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/api")
@Api(value = "Calculadora microservicio")
public class CalculadoraController implements CalculadoraApi {

	@Autowired
	CalculadoraService calculadoraService;

	@Autowired
	UtilsTrace utilsTrace;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@PostMapping(value = "/suma", produces = {MediaType.APPLICATION_JSON_VALUE})
	@ApiOperation(value = "Suma sumando1 y sumando2 del body", notes = "Devuelve la suma")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 400, message = "Bad Request", response = String.class),
			@ApiResponse(code = 404, message = "Not Found", response = String.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = String.class)})
	public String suma(
			@ApiParam(value = "JSON con sumando1 y sumando2", required = true) @Valid @RequestBody SumaDto sumaDto) {

		log.log(Level.INFO, "Sumamos " + sumaDto.getSumando1() + " + "
				+ sumaDto.getSumando2());
		String suma = calculadoraService.suma(sumaDto);

		utilsTrace.trace(suma);

		return suma;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@PostMapping(value = "/resta", produces = {
			MediaType.APPLICATION_JSON_VALUE})
	@ApiOperation(value = "Resta minuendo y sustraendo del body", notes = "Devuelve la resta")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = String.class),
			@ApiResponse(code = 400, message = "Bad Request", response = String.class),
			@ApiResponse(code = 404, message = "Not Found", response = String.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = String.class)})
	public String resta(
			@ApiParam(value = "JSON con minuendo y sustraendo", required = true) @Valid @RequestBody RestaDto restaDto) {

		log.log(Level.INFO, "Restamos " + restaDto.getMinuendo() + " - "
				+ restaDto.getSustraendo());
		String resta = calculadoraService.resta(restaDto);

		utilsTrace.trace(resta);

		return resta;
	}

}
