package com.prueba.calculadora.dto.constantes;

public class Constants {

	private Constants() {

	}

	public static final class Errors {

		private Errors() {

		}

		/* Errores de validacion */
		public static final String ERROR_SUMANDO1_VACIO = "El primer sumando no puede ser vacio";
		public static final String ERROR_SUMANDO1_NULO = "El segundo sumando no puede ser null";

		public static final String ERROR_SUMANDO2_VACIO = "El segundo sumando no puede ser vacio";
		public static final String ERROR_SUMANDO2_NULO = "El segundo sumando no puede ser null";

		public static final String ERROR_MINUENDO_VACIO = "El minuendo no puede ser vacio";
		public static final String ERROR_MINUENDO_NULO = "El minuendo no puede ser nulo";

		public static final String ERROR_SUSTRAENDO_VACIO = "El sustraendo no puede ser vacio";
		public static final String ERROR_SUSTRAENDO_NULO = "El sustraendo no puede ser nulo";

	}

}
