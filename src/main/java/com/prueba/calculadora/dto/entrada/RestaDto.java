package com.prueba.calculadora.dto.entrada;

import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_MINUENDO_NULO;
import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_MINUENDO_VACIO;
import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_SUSTRAENDO_NULO;
import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_SUSTRAENDO_VACIO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Clase con los elementos necesarios(minuendo y sustraendo) para hacer una
 * resta de 2 enteros.
 *
 */
@AllArgsConstructor
@Getter
@ApiModel("Modelo Resta")
public class RestaDto {

	@NotNull(message = ERROR_MINUENDO_NULO)
	@NotBlank(message = ERROR_MINUENDO_VACIO)
	@ApiModelProperty(value = "minuendo", required = true, example = "2", position = 1)
	private String minuendo;

	@NotNull(message = ERROR_SUSTRAENDO_NULO)
	@NotBlank(message = ERROR_SUSTRAENDO_VACIO)
	@ApiModelProperty(value = "sustraendo", required = true, example = "1", position = 2)
	private String sustraendo;

}
