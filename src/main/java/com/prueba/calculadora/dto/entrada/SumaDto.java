package com.prueba.calculadora.dto.entrada;

import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_SUMANDO1_NULO;
import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_SUMANDO1_VACIO;
import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_SUMANDO2_NULO;
import static com.prueba.calculadora.dto.constantes.Constants.Errors.ERROR_SUMANDO2_VACIO;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Clase con los elementos necesarios(sumando1 y sumando2) para hacer una suma
 * de 2 enteros.
 * 
 * @author jmfc7
 *
 */
@ApiModel("Modelo Suma")
@AllArgsConstructor
@Getter
public class SumaDto {

	@NotNull(message = ERROR_SUMANDO1_NULO)
	@NotBlank(message = ERROR_SUMANDO1_VACIO)
	@ApiModelProperty(value = "sumando1", required = true, example = "1", position = 1)
	private String sumando1;

	@NotNull(message = ERROR_SUMANDO2_NULO)
	@NotBlank(message = ERROR_SUMANDO2_VACIO)
	@ApiModelProperty(value = "sumando2", required = true, example = "1", position = 2)
	private String sumando2;

}
