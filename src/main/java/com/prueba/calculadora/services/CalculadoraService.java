package com.prueba.calculadora.services;

import com.prueba.calculadora.dto.entrada.RestaDto;
import com.prueba.calculadora.dto.entrada.SumaDto;

/**
 * Interface con las operaciones disponibles para una calculadora
 * 
 * @author jmfc7
 *
 */
public interface CalculadoraService {

	/**
	 * Metodo para sumar dos numeros enteros
	 * 
	 * @param sumaDto
	 *            contiene el sumando1 y el sumando2
	 * @return suma
	 */
	String suma(SumaDto sumaDto);

	/**
	 * Metodo para restar dos numeros enteros
	 * 
	 * @param restaDto
	 *            contiene el minuendo y el sustraendo
	 * @return resta de minuendo menos sustraendo
	 */
	String resta(RestaDto restaDto);

}
