package com.prueba.calculadora.services.impl;

import org.springframework.stereotype.Service;

import com.prueba.calculadora.dto.entrada.RestaDto;
import com.prueba.calculadora.dto.entrada.SumaDto;
import com.prueba.calculadora.services.CalculadoraService;

@Service
public class CalculadoraServiceImpl implements CalculadoraService {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String suma(SumaDto sumaDto) {
		return String.valueOf(Integer.valueOf(sumaDto.getSumando1())
				+ Integer.valueOf(sumaDto.getSumando2()));
	}

	@Override
	public String resta(RestaDto restaDto) {
		return String.valueOf(Integer.valueOf(restaDto.getMinuendo())
				- Integer.valueOf(restaDto.getSustraendo()));
	}

}
