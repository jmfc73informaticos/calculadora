package com.prueba.calculadora.swagger;

import static springfox.documentation.builders.PathSelectors.regex;

import java.lang.reflect.WildcardType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

	@Autowired
	private TypeResolver typeResolver;

	@Bean
	public Docket calculadoraApi() {

		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(calculadoraApiInfo()).select()
				.paths(calculadoraPaths())
				.apis(RequestHandlerSelectors
						.basePackage("com.prueba.calculadora.controller"))
				.build().pathMapping("/")
				.genericModelSubstitutes(ResponseEntity.class)
				.alternateTypeRules(AlternateTypeRules.newRule(
						typeResolver.resolve(DeferredResult.class,
								typeResolver.resolve(ResponseEntity.class,
										WildcardType.class)),
						typeResolver.resolve(WildcardType.class)))
				.useDefaultResponseMessages(false)
				.apiInfo(calculadoraApiInfo());

	}

	private ApiInfo calculadoraApiInfo() {

		return new ApiInfoBuilder()

				.title("Microservicio Calculadora").version("0.0.1-SNAPSHOT")
				.license("Apache License Version 2.0").build();

	}

	private Predicate<String> calculadoraPaths() {

		return regex("/api/.*");

	}

}