package com.prueba.calculadora.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prueba.calculadora.dto.entrada.RestaDto;
import com.prueba.calculadora.dto.entrada.SumaDto;
import com.prueba.calculadora.mocks.RestaDtoMockFactories;
import com.prueba.calculadora.mocks.SumaDtoMockFactories;
import com.prueba.calculadora.services.impl.CalculadoraServiceImpl;
import com.prueba.calculadora.services.utils.UtilsTrace;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {CalculadoraController.class,
		CalculadoraServiceImpl.class, UtilsTrace.class})
public class CalculadoraControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private UtilsTrace utilsTrace;

	@Mock
	private CalculadoraServiceImpl calculadoraServiceImpl;

	@InjectMocks
	private CalculadoraController calculadoraController;

	@Test
	void testSuma_Ok() throws Exception {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaOk();

		when(calculadoraServiceImpl.suma(sumaDto)).thenReturn("3");

		doNothing().when(utilsTrace).trace(Mockito.any());

		String result = calculadoraController.suma(sumaDto);

		Assertions.assertEquals("3", result);
	}

	@Test
	void testSuma_BodyValido() throws Exception {
		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaOk();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(sumaDto);

		mockMvc.perform(
				post("/api/suma").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isOk());

	}

	@Test
	void testSuma_Sumando1Vacio() throws Exception {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando1Vacio();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(sumaDto);

		mockMvc.perform(
				post("/api/suma").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

	@Test
	void testSuma_Sumando2Vacio() throws Exception {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando2Vacio();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(sumaDto);

		mockMvc.perform(
				post("/api/suma").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

	@Test
	void testSuma_Sumando1Nulo() throws Exception {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando1Nulo();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(sumaDto);

		mockMvc.perform(
				post("/api/suma").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

	@Test
	void testSuma_Sumando2Nulo() throws Exception {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando2Nulo();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(sumaDto);

		mockMvc.perform(
				post("/api/suma").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

	@Test
	void testResta_Ok() {

		RestaDto restaDto = RestaDtoMockFactories.createMockRestaOk();

		when(calculadoraServiceImpl.resta(restaDto)).thenReturn("1");

		doNothing().when(utilsTrace).trace(Mockito.any());

		String result = calculadoraController.resta(restaDto);

		Assertions.assertEquals("1", result);
	}

	@Test
	void testResta_MinuendoVacio() throws Exception {

		RestaDto restaDto = RestaDtoMockFactories.creaMockRestaMinuendoVacio();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(restaDto);

		mockMvc.perform(
				post("/api/resta").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

	@Test
	void testResta_SustraendoVacio() throws Exception {

		RestaDto restaDto = RestaDtoMockFactories
				.creaMockRestaSustraendoVacio();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(restaDto);

		mockMvc.perform(
				post("/api/resta").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

	@Test
	void testResta_MinuendoNulo() throws Exception {

		RestaDto restaDto = RestaDtoMockFactories.creaMockRestaMinuendoNulo();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(restaDto);

		mockMvc.perform(
				post("/api/resta").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

	@Test
	void testResta_SustraendoNulo() throws Exception {

		RestaDto restaDto = RestaDtoMockFactories.creaMockRestaSustraendoNulo();

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(restaDto);

		mockMvc.perform(
				post("/api/resta").contentType(MediaType.APPLICATION_JSON)
						.content(json).characterEncoding("UTF-8"))
				.andExpect(status().isBadRequest());
	}

}
