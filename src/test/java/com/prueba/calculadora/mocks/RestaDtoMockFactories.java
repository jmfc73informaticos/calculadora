package com.prueba.calculadora.mocks;

import com.prueba.calculadora.dto.entrada.RestaDto;

public class RestaDtoMockFactories {

	public static RestaDto createMockRestaOk() {

		return new RestaDto("2", "1");
	}

	public static RestaDto creaMockRestaMinuendoVacio() {

		return new RestaDto("", "2");
	}

	public static RestaDto creaMockRestaMinuendoNulo() {

		return new RestaDto(null, "2");
	}

	public static RestaDto creaMockRestaSustraendoVacio() {

		return new RestaDto("1", "");
	}

	public static RestaDto creaMockRestaSustraendoNulo() {

		return new RestaDto("1", null);
	}

}
