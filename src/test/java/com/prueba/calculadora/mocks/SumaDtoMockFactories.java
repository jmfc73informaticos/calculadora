package com.prueba.calculadora.mocks;

import com.prueba.calculadora.dto.entrada.SumaDto;

public class SumaDtoMockFactories {

	public static SumaDto creaMockSumaOk() {

		return new SumaDto("1", "2");
	}

	public static SumaDto creaMockSumaSumando1Vacio() {

		return new SumaDto("", "2");
	}

	public static SumaDto creaMockSumaSumando2Vacio() {

		return new SumaDto("1", "");
	}

	public static SumaDto creaMockSumaSumando1Nulo() {

		return new SumaDto(null, "2");
	}

	public static SumaDto creaMockSumaSumando2Nulo() {

		return new SumaDto("1", null);
	}

}
