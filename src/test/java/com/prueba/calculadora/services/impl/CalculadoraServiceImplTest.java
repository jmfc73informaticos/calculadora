package com.prueba.calculadora.services.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import com.prueba.calculadora.dto.entrada.RestaDto;
import com.prueba.calculadora.dto.entrada.SumaDto;
import com.prueba.calculadora.mocks.RestaDtoMockFactories;
import com.prueba.calculadora.mocks.SumaDtoMockFactories;

@SpringBootTest
public class CalculadoraServiceImplTest {

	@InjectMocks
	private CalculadoraServiceImpl calculadoraServiceImpl;

	@Test
	public void testSuma_Ok() {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaOk();

		String result = calculadoraServiceImpl.suma(sumaDto);

		Assertions.assertEquals("3", result);
	}

	@Test
	public void testSuma_Sumando1Vacio() {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando1Vacio();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.suma(sumaDto);
		});
	}

	@Test
	public void testSuma_Sumando2Vacio() {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando2Vacio();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.suma(sumaDto);
		});

	}

	@Test
	public void testSuma_Sumando1Nulo() {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando1Nulo();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.suma(sumaDto);
		});
	}

	@Test
	public void testSuma_Sumando2Nulo() {

		SumaDto sumaDto = SumaDtoMockFactories.creaMockSumaSumando2Nulo();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.suma(sumaDto);
		});
	}

	@Test
	public void testResta_Ok() {

		RestaDto restaDto = RestaDtoMockFactories.createMockRestaOk();

		String result = calculadoraServiceImpl.resta(restaDto);

		Assertions.assertEquals("1", result);
	}

	@Test
	public void testResta_MinuendoVacio() {

		RestaDto restaDto = RestaDtoMockFactories.creaMockRestaMinuendoVacio();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.resta(restaDto);
		});
	}

	@Test
	public void testResta_MinuendoNulo() {

		RestaDto restaDto = RestaDtoMockFactories.creaMockRestaMinuendoNulo();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.resta(restaDto);
		});

	}

	@Test
	public void testResta_SustraendoVacio() {

		RestaDto restaDto = RestaDtoMockFactories
				.creaMockRestaSustraendoVacio();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.resta(restaDto);
		});
	}

	@Test
	public void testResta_SustraendoNulo() {

		RestaDto restaDto = RestaDtoMockFactories.creaMockRestaSustraendoNulo();

		Assertions.assertThrows(NumberFormatException.class, () -> {
			calculadoraServiceImpl.resta(restaDto);
		});
	}
}
